import request from "request";
import { Repo } from "./Repo";
import { User } from "./User";


const OPTIONS: any = {
    headers: {
        'User-Agent': 'request'
    },
    json: true
}

export class GithubAPIService {
    getUserInfo(userName: string, cb: (user: User) => any) {

        request.get(`https://api.github.com/users/${userName}`, OPTIONS, (error: any, res: any, body: any) => {
            const user = new User(body)
            cb(user)
        })

    }

    getRepos(userName: string, cb: (repos: Repo[]) => any) {

        request.get(`https://api.github.com/users/${userName}/repos`, OPTIONS, (error: any, res: any, body: any) => {
            const repos = body.map((repo: any) => new Repo(repo))
            cb(repos)
        })

    }
}