import { GithubAPIService } from "./GithubAPIService";
import { Repo } from "./Repo";
import { User } from "./User";
import * as _ from "lodash";


let svc = new GithubAPIService()

if (process.argv.length < 3) {
    console.log('Please pass the username as an argument')
} else {
    const username = process.argv[2]

    svc.getUserInfo(username, (user: User) => {
        svc.getRepos(username, (repos: Repo[]) => {
            const sortedRepos = _.sortBy(repos, [(repo) => repo.forkCount * -1])
            const filteredRepos = _.take(sortedRepos, 5)        
            user.repos = filteredRepos

            console.log(user)
        })
    })
}

        

