"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GithubAPIService = void 0;
const request_1 = __importDefault(require("request"));
const Repo_1 = require("./Repo");
const User_1 = require("./User");
const OPTIONS = {
    headers: {
        'User-Agent': 'request'
    },
    json: true
};
class GithubAPIService {
    getUserInfo(userName, cb) {
        request_1.default.get(`https://api.github.com/users/${userName}`, OPTIONS, (error, res, body) => {
            const user = new User_1.User(body);
            cb(user);
        });
    }
    getRepos(userName, cb) {
        request_1.default.get(`https://api.github.com/users/${userName}/repos`, OPTIONS, (error, res, body) => {
            const repos = body.map((repo) => new Repo_1.Repo(repo));
            cb(repos);
        });
    }
}
exports.GithubAPIService = GithubAPIService;
